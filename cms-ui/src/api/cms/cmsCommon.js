import request from '@/utils/request'


// 查询栏目分类树
export function getCategoryTree() {
  return request({
    url: '/cms/category/tree/list',
    method: 'get'
  })
}
