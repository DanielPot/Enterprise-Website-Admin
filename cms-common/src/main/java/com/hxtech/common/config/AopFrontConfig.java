package com.hxtech.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author Created by Lin Weihong
 * @date on 2024/1/14 16:57
 */
@Configuration
@EnableAspectJAutoProxy
public class AopFrontConfig {
}
