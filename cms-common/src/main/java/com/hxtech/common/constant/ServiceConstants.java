package com.hxtech.common.constant;

/**
 * 业务常量
 *
 * @author Created by Vethong
 * @date on 2024/1/13 15:26
 */
public interface ServiceConstants {
    /**
     * 是否是叶子节点,0不是 1是
     */
    public Long isLeaf = 1L;
    public Long isNotLeaf = 0L;

    public Long ignoreFromAop = 1L;

}
