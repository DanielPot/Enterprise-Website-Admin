package com.hxtech.front.controller;

import com.hxtech.cms.domain.bo.CmsContentBo;
import com.hxtech.cms.domain.vo.CmsContentVo;
import com.hxtech.cms.service.ICmsContentService;
import com.hxtech.common.annotation.RateLimiter;
import com.hxtech.common.core.domain.PageQuery;
import com.hxtech.common.core.domain.R;
import com.hxtech.common.enums.LimitType;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 官网查询控制层
 * @author Created by Lin Weihong
 * @date on 2024/1/8 23:46
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/front/cms")
public class FrontCmsController {

   private final ICmsContentService iCmsContentService;

    /**
     * 获取文章内容,按照栏目分类,同时返回栏目介绍
     * @param bo
     * @param pageQuery
     * @return
     */
    @GetMapping("/page/list")
    public R<Map<String,Object>> list(CmsContentBo bo, PageQuery pageQuery) {
        return R.ok(iCmsContentService.queryPageListAndIntro(bo, pageQuery));
    }

    /**
     * 获取文章内容详细信息
     *
     * @param contentId 主键
     */
    @GetMapping("/content/{contentId}")
    @RateLimiter(limitType = LimitType.IP,time = 10,count = 3)
    public R<CmsContentVo> getInfo(@NotNull(message = "主键不能为空")
                                   @PathVariable Long contentId) {
        return R.ok(iCmsContentService.queryById(contentId));
    }

    /**
     * 获取首页的页面组装数据集
     * @return
     */
    public R<List<CmsContentVo>> listAll() {
        return R.ok(iCmsContentService.queryComplexAllInIndexPage());
    }



}
