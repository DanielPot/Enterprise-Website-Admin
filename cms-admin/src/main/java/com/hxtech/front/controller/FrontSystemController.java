package com.hxtech.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 前台展示系统相关内容控制层
 * @author Created by Lin Weihong
 * @date on 2024/1/8 23:51
 */

@RequiredArgsConstructor
@RestController
@RequestMapping("/front/sys")
public class FrontSystemController {
}
