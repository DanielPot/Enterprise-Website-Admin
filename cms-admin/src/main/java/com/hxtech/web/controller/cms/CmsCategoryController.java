package com.hxtech.web.controller.cms;

import java.util.List;
import java.util.Arrays;

import cn.hutool.core.lang.tree.Tree;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hxtech.common.annotation.RepeatSubmit;
import com.hxtech.common.annotation.Log;
import com.hxtech.common.core.controller.BaseController;
import com.hxtech.common.core.domain.PageQuery;
import com.hxtech.common.core.domain.R;
import com.hxtech.common.core.validate.AddGroup;
import com.hxtech.common.core.validate.EditGroup;
import com.hxtech.common.enums.BusinessType;
import com.hxtech.common.utils.poi.ExcelUtil;
import com.hxtech.cms.domain.vo.CmsCategoryVo;
import com.hxtech.cms.domain.bo.CmsCategoryBo;
import com.hxtech.cms.service.ICmsCategoryService;
import com.hxtech.common.core.page.TableDataInfo;

/**
 * 栏目分类
 *
 * @author Vethong
 * @date 2024-01-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/cms/category")
public class CmsCategoryController extends BaseController {

    private final ICmsCategoryService iCmsCategoryService;

    /**
     * 查询栏目分类列表
     */
    @SaCheckPermission("cms:category:list")
    @GetMapping("/list")
    public TableDataInfo<CmsCategoryVo> list(CmsCategoryBo bo, PageQuery pageQuery) {
        return iCmsCategoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出栏目分类列表
     */
    @SaCheckPermission("cms:category:export")
    @Log(title = "栏目分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsCategoryBo bo, HttpServletResponse response) {
        List<CmsCategoryVo> list = iCmsCategoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "栏目分类", CmsCategoryVo.class, response);
    }

    /**
     * 获取栏目分类详细信息
     *
     * @param categoryId 主键
     */
    @SaCheckPermission("cms:category:query")
    @GetMapping("/{categoryId}")
    public R<CmsCategoryVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long categoryId) {
        return R.ok(iCmsCategoryService.queryById(categoryId));
    }

    /**
     * 新增栏目分类
     */
    @SaCheckPermission("cms:category:add")
    @Log(title = "栏目分类", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsCategoryBo bo) {
        return toAjax(iCmsCategoryService.insertByBo(bo));
    }

    /**
     * 修改栏目分类
     */
    @SaCheckPermission("cms:category:edit")
    @Log(title = "栏目分类", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsCategoryBo bo) {
        return toAjax(iCmsCategoryService.updateByBo(bo));
    }

    /**
     * 删除栏目分类
     *
     * @param categoryIds 主键串
     */
    @SaCheckPermission("cms:category:remove")
    @Log(title = "栏目分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{categoryIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] categoryIds) {
        return toAjax(iCmsCategoryService.deleteWithValidByIds(Arrays.asList(categoryIds), true));
    }

    /**
     * 获取栏目分类数
     *
     */
    @SaCheckPermission("cms:category:query")
    @GetMapping("/tree/list")
    public R<List<Tree<Long>>> getTreeList() {
        return R.ok(iCmsCategoryService.queryTreeList());
    }
}
