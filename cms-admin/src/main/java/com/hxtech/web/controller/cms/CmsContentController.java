package com.hxtech.web.controller.cms;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hxtech.common.annotation.RepeatSubmit;
import com.hxtech.common.annotation.Log;
import com.hxtech.common.core.controller.BaseController;
import com.hxtech.common.core.domain.PageQuery;
import com.hxtech.common.core.domain.R;
import com.hxtech.common.core.validate.AddGroup;
import com.hxtech.common.core.validate.EditGroup;
import com.hxtech.common.enums.BusinessType;
import com.hxtech.common.utils.poi.ExcelUtil;
import com.hxtech.cms.domain.vo.CmsContentVo;
import com.hxtech.cms.domain.bo.CmsContentBo;
import com.hxtech.cms.service.ICmsContentService;
import com.hxtech.common.core.page.TableDataInfo;

/**
 * 文章内容
 *
 * @author Vethong
 * @date 2024-01-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/cms/content")
public class CmsContentController extends BaseController {

    private final ICmsContentService iCmsContentService;

    /**
     * 查询文章内容列表
     */
    @SaCheckPermission("cms:content:list")
    @GetMapping("/list")
    public TableDataInfo<CmsContentVo> list(CmsContentBo bo, PageQuery pageQuery) {
        return iCmsContentService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出文章内容列表
     */
    @SaCheckPermission("cms:content:export")
    @Log(title = "文章内容", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsContentBo bo, HttpServletResponse response) {
        List<CmsContentVo> list = iCmsContentService.queryList(bo);
        ExcelUtil.exportExcel(list, "文章内容", CmsContentVo.class, response);
    }

    /**
     * 获取文章内容详细信息
     *
     * @param contentId 主键
     */
    @SaCheckPermission("cms:content:query")
    @GetMapping("/{contentId}")
    public R<CmsContentVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long contentId) {
        return R.ok(iCmsContentService.queryById(contentId));
    }

    /**
     * 新增文章内容
     */
    @SaCheckPermission("cms:content:add")
    @Log(title = "文章内容", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsContentBo bo) {
        return toAjax(iCmsContentService.insertByBo(bo));
    }

    /**
     * 修改文章内容
     */
    @SaCheckPermission("cms:content:edit")
    @Log(title = "文章内容", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsContentBo bo) {
        return toAjax(iCmsContentService.updateByBo(bo));
    }

    /**
     * 删除文章内容
     *
     * @param contentIds 主键串
     */
    @SaCheckPermission("cms:content:remove")
    @Log(title = "文章内容", businessType = BusinessType.DELETE)
    @DeleteMapping("/{contentIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] contentIds) {
        return toAjax(iCmsContentService.deleteWithValidByIds(Arrays.asList(contentIds), true));
    }
}
