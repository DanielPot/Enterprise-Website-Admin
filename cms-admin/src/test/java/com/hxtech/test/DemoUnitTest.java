package com.hxtech.test;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.json.JSONObject;
import com.hxtech.cms.domain.vo.CmsCategoryVo;
import com.hxtech.cms.mapper.CmsCategoryMapper;
import com.hxtech.common.config.RuoYiConfig;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 单元测试案例
 *
 * @author Lion Li
 */
@SpringBootTest // 此注解只能在 springboot 主包下使用 需包含 main 方法与 yml 配置文件
@DisplayName("单元测试案例")
public class DemoUnitTest {

    @Autowired
    private RuoYiConfig ruoYiConfig;

    @Autowired
    private CmsCategoryMapper baseMapper;


    @DisplayName("测试 @SpringBootTest @Test @DisplayName 注解")
    @Test
    public void testTest() {
        System.out.println(ruoYiConfig);
    }

    @Test
    public void testTree() {
        List<CmsCategoryVo> cmsCategoryVos = baseMapper.selectVoList(null);
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        treeNodeConfig.setIdKey("categoryId");
        treeNodeConfig.setNameKey("categoryName");
        treeNodeConfig.setWeightKey("categorySort");
        List<Tree<Long>> treeNodes = TreeUtil.build(cmsCategoryVos, 0L, treeNodeConfig, (treeNode, tree) -> {
            tree.setId(treeNode.getCategoryId());
            tree.setParentId(treeNode.getParentId());
            tree.setName(treeNode.getCategoryName());
            tree.setWeight(treeNode.getCategorySort());

            //扩展属性
            tree.putExtra("categoryDescription", treeNode.getCategoryDescription());
            tree.putExtra("categoryDisplay", treeNode.getCategoryDisplay());
            tree.putExtra("categoryKeyword", treeNode.getCategoryKeyword());
            tree.putExtra("del", treeNode.getDel());
            tree.putExtra("isLeaf", treeNode.getIsLeaf());
        });
        System.out.println("treeNodes = " + treeNodes);
    }

    @Disabled
    @DisplayName("测试 @Disabled 注解")
    @Test
    public void testDisabled() {
        System.out.println(ruoYiConfig);
    }

    @Timeout(value = 2L, unit = TimeUnit.SECONDS)
    @DisplayName("测试 @Timeout 注解")
    @Test
    public void testTimeout() throws InterruptedException {
        Thread.sleep(3000);
        System.out.println(ruoYiConfig);
    }


    @DisplayName("测试 @RepeatedTest 注解")
    @RepeatedTest(3)
    public void testRepeatedTest() {
        System.out.println(666);
    }

    @BeforeAll
    public static void testBeforeAll() {
        System.out.println("@BeforeAll ==================");
    }

    @BeforeEach
    public void testBeforeEach() {
        System.out.println("@BeforeEach ==================");
    }

    @AfterEach
    public void testAfterEach() {
        System.out.println("@AfterEach ==================");
    }

    @AfterAll
    public static void testAfterAll() {
        System.out.println("@AfterAll ==================");
    }

}
