package com.hxtech.system.mapper;

import com.hxtech.common.core.mapper.BaseMapperPlus;
import com.hxtech.system.domain.SysRoleDept;

/**
 * 角色与部门关联表 数据层
 *
 * @author Lion Li
 */
public interface SysRoleDeptMapper extends BaseMapperPlus<SysRoleDeptMapper, SysRoleDept, SysRoleDept> {

}
