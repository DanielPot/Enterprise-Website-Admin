package com.hxtech.system.mapper;

import com.hxtech.common.core.mapper.BaseMapperPlus;
import com.hxtech.system.domain.SysUserPost;

/**
 * 用户与岗位关联表 数据层
 *
 * @author Lion Li
 */
public interface SysUserPostMapper extends BaseMapperPlus<SysUserPostMapper, SysUserPost, SysUserPost> {

}
