package com.hxtech.system.mapper;

import com.hxtech.common.core.domain.entity.SysDictType;
import com.hxtech.common.core.mapper.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author Lion Li
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictTypeMapper, SysDictType, SysDictType> {

}
