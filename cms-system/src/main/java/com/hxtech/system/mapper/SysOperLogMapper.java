package com.hxtech.system.mapper;

import com.hxtech.common.core.mapper.BaseMapperPlus;
import com.hxtech.system.domain.SysOperLog;

/**
 * 操作日志 数据层
 *
 * @author Lion Li
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLogMapper, SysOperLog, SysOperLog> {

}
