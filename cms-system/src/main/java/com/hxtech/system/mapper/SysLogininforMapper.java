package com.hxtech.system.mapper;

import com.hxtech.common.core.mapper.BaseMapperPlus;
import com.hxtech.system.domain.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author Lion Li
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininforMapper, SysLogininfor, SysLogininfor> {

}
