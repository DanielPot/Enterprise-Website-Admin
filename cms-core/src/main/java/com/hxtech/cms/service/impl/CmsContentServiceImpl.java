package com.hxtech.cms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hxtech.cms.domain.CmsCategory;
import com.hxtech.cms.domain.vo.CmsCategoryVo;
import com.hxtech.cms.mapper.CmsCategoryMapper;
import com.hxtech.common.core.page.TableDataInfo;
import com.hxtech.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hxtech.common.utils.StringUtils;
import com.hxtech.system.domain.vo.SysOssVo;
import com.hxtech.system.mapper.SysOssMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hxtech.cms.domain.bo.CmsContentBo;
import com.hxtech.cms.domain.vo.CmsContentVo;
import com.hxtech.cms.domain.CmsContent;
import com.hxtech.cms.mapper.CmsContentMapper;
import com.hxtech.cms.service.ICmsContentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * 文章内容Service业务层处理
 *
 * @author Vethong
 * @date 2024-01-08
 */
@RequiredArgsConstructor
@Service
public class CmsContentServiceImpl implements ICmsContentService {

    private final CmsContentMapper baseMapper;
    private final CmsCategoryMapper cmsCategoryMapper;
    private final SysOssMapper sysOssMapper;

    /**
     * 查询文章内容
     */
    @Override
    public CmsContentVo queryById(Long contentId){
        return baseMapper.selectVoById(contentId);
    }

    /**
     * 查询文章内容列表
     */
    @Override
    public TableDataInfo<CmsContentVo> queryPageList(CmsContentBo bo, PageQuery pageQuery) {
        List<CmsCategoryVo> cmsCategoryVos = cmsCategoryMapper.selectVoList2();
        Map<Long, String> categoryIdAndNameMap = cmsCategoryVos.stream().collect(Collectors.toMap(CmsCategoryVo::getCategoryId, CmsCategoryVo::getCategoryName));
        LambdaQueryWrapper<CmsContent> lqw = buildQueryWrapper(bo);
        Page<CmsContentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        List<CmsContentVo> records = result.getRecords();
        List<CmsContentVo> transferCmsContentVos = records.stream().map(cmsContentVo -> {
            if (cmsContentVo.getCategoryId() != 0) {
                cmsContentVo.setCategoryName(categoryIdAndNameMap.get(cmsContentVo.getCategoryId()));
            }
            return cmsContentVo;
        }).collect(Collectors.toList());
        result.setRecords(transferCmsContentVos);
        return TableDataInfo.build(result);
    }

    /**
     * 查询文章内容列表
     */
    @Override
    public Map<String,Object> queryPageListAndIntro(CmsContentBo bo, PageQuery pageQuery) {
        List<SysOssVo> sysOssVos = sysOssMapper.selectVoList(null);
        Map<Long, String> ossMap = sysOssVos.stream().collect(Collectors.toMap(SysOssVo::getOssId, SysOssVo::getUrl));
        CmsCategoryVo cmsCategoryVo = cmsCategoryMapper.selectVoById(bo.getCategoryId());
        String categoryIntroduction = "";
        if (ObjectUtil.isNotEmpty(cmsCategoryVo)) {
            //获取栏目介绍
            categoryIntroduction = cmsCategoryVo.getCategoryDescription();
        }
        LambdaQueryWrapper<CmsContent> lqw = buildQueryWrapper(bo);
        Page<CmsContentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        List<CmsContentVo> collect = result.getRecords().stream().peek(cmsContentVo -> {
            if (cmsContentVo.getContentImg() != null) {
                cmsContentVo.setContentImgUrl(ossMap.get(Long.parseLong(cmsContentVo.getContentImg())));
            }
        }).collect(Collectors.toList());
        result.setRecords(collect);
        Map<String,Object> map = new HashMap<>();
        map.put("records", result);
        map.put("categoryIntro",categoryIntroduction);
        return map;
    }

    /**
     * 查询文章内容列表
     */
    @Override
    public List<CmsContentVo> queryList(CmsContentBo bo) {
        LambdaQueryWrapper<CmsContent> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsContent> buildQueryWrapper(CmsContentBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsContent> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getContentTitle()), CmsContent::getContentTitle, bo.getContentTitle());
        lqw.like(StringUtils.isNotBlank(bo.getContentShortTitle()), CmsContent::getContentShortTitle, bo.getContentShortTitle());
        lqw.like(StringUtils.isNotBlank(bo.getContentKeyword()), CmsContent::getContentKeyword, bo.getContentKeyword());
        lqw.like(StringUtils.isNotBlank(bo.getContentDescription()), CmsContent::getContentDescription, bo.getContentDescription());
        lqw.like(StringUtils.isNotBlank(bo.getContentSource()), CmsContent::getContentSource, bo.getContentSource());
        lqw.eq(StringUtils.isNotBlank(bo.getContentImg()), CmsContent::getContentImg, bo.getContentImg());
        lqw.like(StringUtils.isNotBlank(bo.getContentDetails()), CmsContent::getContentDetails, bo.getContentDetails());
        lqw.eq(bo.getContentHit() != null, CmsContent::getContentHit, bo.getContentHit());
        lqw.eq(bo.getContentSort() != null, CmsContent::getContentSort, bo.getContentSort());
        lqw.eq(bo.getContentDisplay() != null, CmsContent::getContentDisplay, bo.getContentDisplay());
        lqw.eq(bo.getIsPublished() != null, CmsContent::getIsPublished, bo.getIsPublished());
        lqw.eq(bo.getCategoryId() != null, CmsContent::getCategoryId, bo.getCategoryId());
        return lqw;
    }

    /**
     * 新增文章内容
     */
    @Override
    public Boolean insertByBo(CmsContentBo bo) {
        CmsContent add = BeanUtil.toBean(bo, CmsContent.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setContentId(add.getContentId());
        }
        return flag;
    }

    /**
     * 修改文章内容
     */
    @Override
    public Boolean updateByBo(CmsContentBo bo) {
        CmsContent update = BeanUtil.toBean(bo, CmsContent.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsContent entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除文章内容
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }


    @Override
    public List<CmsContentVo> queryComplexAllInIndexPage() {
        return null;
    }
}
