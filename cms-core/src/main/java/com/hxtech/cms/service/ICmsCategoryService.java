package com.hxtech.cms.service;

import cn.hutool.core.lang.tree.Tree;
import com.hxtech.cms.domain.CmsCategory;
import com.hxtech.cms.domain.vo.CmsCategoryVo;
import com.hxtech.cms.domain.bo.CmsCategoryBo;
import com.hxtech.common.core.page.TableDataInfo;
import com.hxtech.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 栏目分类Service接口
 *
 * @author Vethong
 * @date 2024-01-08
 */
public interface ICmsCategoryService {

    /**
     * 查询栏目分类
     */
    CmsCategoryVo queryById(Long categoryId);

    /**
     * 查询栏目分类列表
     */
    TableDataInfo<CmsCategoryVo> queryPageList(CmsCategoryBo bo, PageQuery pageQuery);

    /**
     * 查询栏目分类列表
     */
    List<CmsCategoryVo> queryList(CmsCategoryBo bo);

    /**
     * 新增栏目分类
     */
    Boolean insertByBo(CmsCategoryBo bo);

    /**
     * 修改栏目分类
     */
    Boolean updateByBo(CmsCategoryBo bo);

    /**
     * 校验并批量删除栏目分类信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 获取栏目分类树
     */
    List<Tree<Long>> queryTreeList();
}
