package com.hxtech.cms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.hxtech.cms.domain.CmsContent;
import com.hxtech.cms.mapper.CmsContentMapper;
import com.hxtech.common.constant.ServiceConstants;
import com.hxtech.common.core.page.TableDataInfo;
import com.hxtech.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hxtech.common.exception.ServiceException;
import com.hxtech.common.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hxtech.cms.domain.bo.CmsCategoryBo;
import com.hxtech.cms.domain.vo.CmsCategoryVo;
import com.hxtech.cms.domain.CmsCategory;
import com.hxtech.cms.mapper.CmsCategoryMapper;
import com.hxtech.cms.service.ICmsCategoryService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 栏目分类Service业务层处理
 *
 * @author Vethong
 * @date 2024-01-08
 */
@RequiredArgsConstructor
@Service
public class CmsCategoryServiceImpl implements ICmsCategoryService {

    private final CmsCategoryMapper baseMapper;
    private final CmsContentMapper cmsContentMapper;

    /**
     * 查询栏目分类
     */
    @Override
    public CmsCategoryVo queryById(Long categoryId){
        return baseMapper.selectVoById(categoryId);
    }

    /**
     * 查询栏目分类列表
     */
    @Override
    public TableDataInfo<CmsCategoryVo> queryPageList(CmsCategoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsCategory> lqw = buildQueryWrapper(bo);
        Page<CmsCategoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询栏目分类列表
     */
    @Override
    public List<CmsCategoryVo> queryList(CmsCategoryBo bo) {
        LambdaQueryWrapper<CmsCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsCategory> buildQueryWrapper(CmsCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsCategory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getParentId() != null, CmsCategory::getParentId, bo.getParentId());
        lqw.eq(bo.getIsLeaf() != null, CmsCategory::getIsLeaf, bo.getIsLeaf());
        lqw.eq(bo.getCategoryDisplay() != null, CmsCategory::getCategoryDisplay, bo.getCategoryDisplay());
        lqw.like(StringUtils.isNotBlank(bo.getCategoryName()), CmsCategory::getCategoryName, bo.getCategoryName());
        lqw.like(StringUtils.isNotBlank(bo.getCategoryDescription()), CmsCategory::getCategoryDescription, bo.getCategoryDescription());
        lqw.like(StringUtils.isNotBlank(bo.getCategoryKeyword()), CmsCategory::getCategoryKeyword, bo.getCategoryKeyword());
        lqw.eq(bo.getCategorySort() != null, CmsCategory::getCategorySort, bo.getCategorySort());
        lqw.eq(bo.getDel() != null, CmsCategory::getDel, bo.getDel());
        return lqw;
    }

    /**
     * 新增栏目分类
     */
    @Override
    public Boolean insertByBo(CmsCategoryBo bo) {
        CmsCategory add = BeanUtil.toBean(bo, CmsCategory.class);
        if (add.getParentId() != null) {
            add.setIsLeaf(ServiceConstants.isLeaf);
        }
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCategoryId(add.getCategoryId());
        }
        return flag;
    }

    /**
     * 修改栏目分类
     */
    @Override
    public Boolean updateByBo(CmsCategoryBo bo) {
        CmsCategory update = BeanUtil.toBean(bo, CmsCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsCategory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除栏目分类
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            LambdaQueryWrapper<CmsCategory> lqwCategory = Wrappers.lambdaQuery();
            lqwCategory.in(CmsCategory::getParentId,ids);
            boolean existsChildrenCategory = baseMapper.exists(lqwCategory);
            if (existsChildrenCategory){
                throw new ServiceException("当前栏目下已有子栏目，无法删除");
            }
            LambdaQueryWrapper<CmsContent> lqw = Wrappers.lambdaQuery();
            lqw.in(ids.size() > 0, CmsContent::getCategoryId,ids);
            boolean existsContent = cmsContentMapper.exists(lqw);
            if (existsContent){
                throw new ServiceException("当前栏目下已有关联文章，无法删除");
            }
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 获取栏目分类树
     * @return
     */
    @Override
    public List<Tree<Long>> queryTreeList() {
        List<CmsCategoryVo> cmsCategoryVos = baseMapper.selectVoList2();
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        treeNodeConfig.setIdKey("categoryId");
        treeNodeConfig.setNameKey("categoryName");
        treeNodeConfig.setWeightKey("categorySort");
        List<Tree<Long>> treeNodes = TreeUtil.build(cmsCategoryVos, 0L, treeNodeConfig, ((treeNode, tree) -> {
            tree.setId(treeNode.getCategoryId());
            tree.setParentId(treeNode.getParentId());
            tree.setName(treeNode.getCategoryName());
            tree.setWeight(treeNode.getCategorySort());

            //扩展属性
            tree.putExtra("categoryDescription", treeNode.getCategoryDescription());
            tree.putExtra("categoryDisplay", treeNode.getCategoryDisplay());
            tree.putExtra("categoryKeyword", treeNode.getCategoryKeyword());
            tree.putExtra("del", treeNode.getDel());
            tree.putExtra("isLeaf", treeNode.getIsLeaf());
            tree.putExtra("parentCategoryName", treeNode.getParentCategoryName());
            }));
        return treeNodes;
    }
}
