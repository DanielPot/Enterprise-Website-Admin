package com.hxtech.cms.service;

import com.hxtech.cms.domain.CmsContent;
import com.hxtech.cms.domain.vo.CmsContentVo;
import com.hxtech.cms.domain.bo.CmsContentBo;
import com.hxtech.common.core.page.TableDataInfo;
import com.hxtech.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 文章内容Service接口
 *
 * @author Vethong
 * @date 2024-01-08
 */
public interface ICmsContentService {

    /**
     * 查询文章内容
     */
    CmsContentVo queryById(Long contentId);

    /**
     * 查询文章内容列表
     */
    TableDataInfo<CmsContentVo> queryPageList(CmsContentBo bo, PageQuery pageQuery);


    /**
     * 查询文章内容列表以及详细信息
     */
    Map<String,Object> queryPageListAndIntro(CmsContentBo bo, PageQuery pageQuery);

    /**
     * 查询文章内容列表
     */
    List<CmsContentVo> queryList(CmsContentBo bo);

    /**
     * 新增文章内容
     */
    Boolean insertByBo(CmsContentBo bo);

    /**
     * 修改文章内容
     */
    Boolean updateByBo(CmsContentBo bo);

    /**
     * 校验并批量删除文章内容信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<CmsContentVo> queryComplexAllInIndexPage();
}
