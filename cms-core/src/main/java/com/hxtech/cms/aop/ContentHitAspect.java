package com.hxtech.cms.aop;

import com.hxtech.cms.domain.bo.CmsContentBo;
import com.hxtech.cms.domain.vo.CmsContentVo;
import com.hxtech.cms.service.ICmsContentService;
import com.hxtech.common.constant.ServiceConstants;
import com.hxtech.common.helper.LoginHelper;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * @author Created by Lin Weihong
 * @date on 2024/1/14 16:51
 */
@Aspect
@Component
@RequiredArgsConstructor
public class ContentHitAspect {

    private final ICmsContentService iCmsContentService;


    @After("execution(* com.hxtech.front.controller.FrontCmsController.getInfo(..)) && args(contentId)")
    public void increaseHitCount(JoinPoint joinPoint, Long contentId) {
        CmsContentVo content = iCmsContentService.queryById(contentId);
        content.setContentHit(content.getContentHit() + 1);
        CmsContentBo cmsContentBo = new CmsContentBo();
        BeanUtils.copyProperties(content, cmsContentBo);
        cmsContentBo.setIgnoreFromAop(ServiceConstants.ignoreFromAop);
        iCmsContentService.updateByBo(cmsContentBo);
    }
}
