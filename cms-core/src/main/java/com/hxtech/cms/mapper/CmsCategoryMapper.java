package com.hxtech.cms.mapper;

import com.hxtech.cms.domain.CmsCategory;
import com.hxtech.cms.domain.vo.CmsCategoryVo;
import com.hxtech.common.core.mapper.BaseMapperPlus;

import java.util.List;

/**
 * 栏目分类Mapper接口
 *
 * @author Vethong
 * @date 2024-01-08
 */
public interface CmsCategoryMapper extends BaseMapperPlus<CmsCategoryMapper, CmsCategory, CmsCategoryVo> {

    List<CmsCategoryVo> selectVoList2();
}
