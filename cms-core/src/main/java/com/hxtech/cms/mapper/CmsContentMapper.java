package com.hxtech.cms.mapper;

import com.hxtech.cms.domain.CmsContent;
import com.hxtech.cms.domain.vo.CmsContentVo;
import com.hxtech.common.core.mapper.BaseMapperPlus;

/**
 * 文章内容Mapper接口
 *
 * @author Vethong
 * @date 2024-01-08
 */
public interface CmsContentMapper extends BaseMapperPlus<CmsContentMapper, CmsContent, CmsContentVo> {

}
