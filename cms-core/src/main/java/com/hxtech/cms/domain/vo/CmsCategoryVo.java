package com.hxtech.cms.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.hxtech.common.annotation.ExcelDictFormat;
import com.hxtech.common.convert.ExcelDictConvert;
import lombok.Data;

import java.io.Serializable;

/**
 * 栏目分类视图对象 cms_category
 *
 * @author Vethong
 * @date 2024-01-08
 */
@Data
@ExcelIgnoreUnannotated
public class CmsCategoryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long categoryId;

    /**
     * 所属栏目(父级栏目)
     */
    @ExcelProperty(value = "所属栏目")
    private Long parentId;

    @ExcelProperty(value = "所属栏目名称")
    @TableField(exist = false)
    private String parentCategoryName;

    /**
     * 是否是叶子节点
     */
    @ExcelProperty(value = "是否是叶子节点", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_or_no_01")
    private Long isLeaf;

    /**
     * 栏目是否显示(0不显示,1显示)
     */
    @ExcelProperty(value = "栏目是否显示", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_or_no_01")
    private Long categoryDisplay;

    /**
     * 栏目名称
     */
    @ExcelProperty(value = "栏目名称")
    @TableField(exist = false)
    private String categoryName;

    /**
     * 栏目描述
     */
    @ExcelProperty(value = "栏目描述")
    private String categoryDescription;

    /**
     * 栏目管理关键字
     */
    @ExcelProperty(value = "栏目管理关键字")
    private String categoryKeyword;

    /**
     * 自定义顺序
     */
    @ExcelProperty(value = "自定义顺序")
    private Long categorySort;

    /**
     * 删除标记
     */
    @ExcelProperty(value = "删除标记", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_or_no_01")
    private Long del;


}
