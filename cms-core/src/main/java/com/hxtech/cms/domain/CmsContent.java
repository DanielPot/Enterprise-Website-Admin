package com.hxtech.cms.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.hxtech.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 文章内容对象 cms_content
 *
 * @author Vethong
 * @date 2024-01-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_content")
public class CmsContent extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    @TableId(value = "content_id")
    private Long contentId;
    /**
     * 文章标题
     */
    private String contentTitle;
    /**
     * 文章副标题
     */
    private String contentShortTitle;
    /**
     * 文章关键字
     */
    private String contentKeyword;
    /**
     * 文章描述
     */
    private String contentDescription;
    /**
     * 文章来源
     */
    private String contentSource;
    /**
     * 文章缩略图
     */
    private String contentImg;
    /**
     * 文章内容
     */
    private String contentDetails;
    /**
     * 文章点击次数
     */
    private Long contentHit;
    /**
     * 自定义顺序
     */
    private Long contentSort;
    /**
     * 是否显示(0不显示,1显示)
     */
    private Long contentDisplay;
    /**
     * 是否发布(0不发布,1发布)
     */
    private Long isPublished;
    /**
     * 所属栏目编码
     */
    private Long categoryId;

}
