package com.hxtech.cms.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.hxtech.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 栏目分类对象 cms_category
 *
 * @author Vethong
 * @date 2024-01-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_category")
public class CmsCategory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    @TableId(value = "category_id")
    private Long categoryId;
    /**
     * 所属栏目(父级栏目)
     */
    private Long parentId;

    /**
     * 所属栏目名称
     */
    @TableField(exist = false)
    private String parentCategoryName;

    /**
     * 是否是叶子节点
     */
    private Long isLeaf;
    /**
     * 栏目是否显示(0不显示,1显示)
     */
    private Long categoryDisplay;
    /**
     * 栏目名称
     */
    private String categoryName;
    /**
     * 栏目描述
     */
    private String categoryDescription;
    /**
     * 栏目管理关键字
     */
    private String categoryKeyword;
    /**
     * 自定义顺序
     */
    private Long categorySort;
    /**
     * 删除标记
     */
    private Long del;

}
