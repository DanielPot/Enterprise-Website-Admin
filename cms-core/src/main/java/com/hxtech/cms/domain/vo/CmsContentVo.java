package com.hxtech.cms.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.hxtech.common.annotation.ExcelDictFormat;
import com.hxtech.common.convert.ExcelDictConvert;
import com.hxtech.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章内容视图对象 cms_content
 *
 * @author Vethong
 * @date 2024-01-08
 */
@Data
@ExcelIgnoreUnannotated
public class CmsContentVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long contentId;

    /**
     * 文章标题
     */
    @ExcelProperty(value = "文章标题")
    private String contentTitle;

    /**
     * 文章副标题
     */
    @ExcelProperty(value = "文章副标题")
    private String contentShortTitle;

    /**
     * 文章关键字
     */
    @ExcelProperty(value = "文章关键字")
    private String contentKeyword;

    /**
     * 文章描述
     */
    @ExcelProperty(value = "文章描述")
    private String contentDescription;

    /**
     * 文章来源
     */
    @ExcelProperty(value = "文章来源")
    private String contentSource;

    /**
     * 文章缩略图
     */
    @ExcelProperty(value = "文章缩略图")
    private String contentImg;

    /**
     * 文章缩略图Url
     */
    @ExcelProperty(value = "文章缩略图Url")
    private String contentImgUrl;

    /**
     * 文章内容
     */
    @ExcelProperty(value = "文章内容")
    private String contentDetails;

    /**
     * 文章点击次数
     */
    @ExcelProperty(value = "文章点击次数")
    private Long contentHit;

    /**
     * 自定义顺序
     */
    @ExcelProperty(value = "自定义顺序")
    private Long contentSort;

    /**
     * 是否显示(0不显示,1显示)
     */
    @ExcelProperty(value = "是否显示(0不显示,1显示)", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_or_no_01")
    private Long contentDisplay;

    /**
     * 是否发布(0不发布,1发布)
     */
    @ExcelProperty(value = "是否发布(0不发布,1发布)", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_or_no_01")
    private Long isPublished;

    /**
     * 所属栏目编码
     */
    @ExcelProperty(value = "所属栏目编码")
    private Long categoryId;

    /**
     * 所属栏目名称
     */
    @ExcelProperty(value = "所属栏目名称")
    private String categoryName;


    /**
     * 创建时间
     */
    private Date createTime;




}
