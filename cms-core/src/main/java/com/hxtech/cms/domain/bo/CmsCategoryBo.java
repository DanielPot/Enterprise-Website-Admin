package com.hxtech.cms.domain.bo;

import com.hxtech.common.core.domain.BaseEntity;
import com.hxtech.common.core.validate.AddGroup;
import com.hxtech.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 栏目分类业务对象 cms_category
 *
 * @author Vethong
 * @date 2024-01-08
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsCategoryBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long categoryId;

    /**
     * 所属栏目(父级栏目)
     */
    private Long parentId;

    /**
     * 是否是叶子节点
     */
//    @NotNull(message = "是否是叶子节点不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long isLeaf;

    /**
     * 栏目是否显示(0不显示,1显示)
     */
    @NotNull(message = "栏目是否显示不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long categoryDisplay;

    /**
     * 栏目名称
     */
    @NotBlank(message = "栏目名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String categoryName;

    /**
     * 栏目描述
     */
    @NotBlank(message = "栏目描述不能为空", groups = { AddGroup.class, EditGroup.class })
    private String categoryDescription;

    /**
     * 栏目管理关键字
     */
    @NotBlank(message = "栏目管理关键字不能为空", groups = { AddGroup.class, EditGroup.class })
    private String categoryKeyword;

    /**
     * 自定义顺序
     */
    @NotNull(message = "栏目顺序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long categorySort;

    /**
     * 删除标记
     */
    private Long del;


}
