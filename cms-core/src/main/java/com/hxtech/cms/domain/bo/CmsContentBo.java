package com.hxtech.cms.domain.bo;

import com.hxtech.common.core.domain.BaseEntity;
import com.hxtech.common.core.validate.AddGroup;
import com.hxtech.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;


/**
 * 文章内容业务对象 cms_content
 *
 * @author Vethong
 * @date 2024-01-08
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsContentBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long contentId;

    /**
     * 文章标题
     */
    @NotBlank(message = "文章标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentTitle;

    /**
     * 文章副标题
     */
    @NotBlank(message = "文章副标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentShortTitle;

    /**
     * 文章关键字
     */
    @NotBlank(message = "文章关键字不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentKeyword;

    /**
     * 文章描述
     */
    @NotBlank(message = "文章描述不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentDescription;

    /**
     * 文章来源
     */
    @NotBlank(message = "文章来源不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentSource;

    /**
     * 文章缩略图
     */
//    @NotBlank(message = "文章缩略图不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentImg;

    /**
     * 文章内容
     */
    @NotBlank(message = "文章内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String contentDetails;

    /**
     * 文章点击次数
     */
//    @NotNull(message = "文章点击次数不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long contentHit;

    /**
     * 自定义顺序
     */
    @NotNull(message = "自定义顺序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long contentSort;

    /**
     * 是否显示(0不显示,1显示)
     */
    @NotNull(message = "是否显示(0不显示,1显示)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long contentDisplay;

    /**
     * 是否发布(0不发布,1发布)
     */
    @NotNull(message = "是否发布(0不发布,1发布)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long isPublished;

    /**
     * 所属栏目编码
     */
    @NotNull(message = "所属栏目编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long categoryId;


}
